import sys
import os
from pathlib import Path
# sys.path.append('../../')
# sys.path.append('../')
# sys.path.append('./')
sys.path.append((Path("./").absolute() / 'code_switching_machine_translation').as_posix())
# print(os.getcwd())
import config  # noqa: F401
